package com.pharmapartners.collaboration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Do you like wonderful easy to read code? What do you think about this nice little peace?
 */
@SpringBootApplication
public class CollaborationApplication {

    public static void main(String[] args) {
        SpringApplication.run(CollaborationApplication.class, args);

        InternshipRepository internshipRepository = new InternshipRepository();
        Student student = new Student();
        PharmaPartners pharmaPartners = new PharmaPartners();

        if (student.isInterested()) {
            String emailAddress = student.givesEmailAddress();
            pharmaPartners.sendInternshipAssignments(emailAddress);
            if (student.isInterested()) {
                pharmaPartners.arrangeMeeting(emailAddress);
                if (student.isInterested()) {
                    pharmaPartners.setupInternshipAgreement(emailAddress);
                    internshipRepository.save(student);
                    pharmaPartners.appreciation();
                } else {
                    student.hopefullyGivesFeedback();
                    pharmaPartners.appreciation();
                }
            } else {
                student.hopefullyGivesFeedback();
                pharmaPartners.appreciation();
            }
        } else {
            student.hopefullyGivesFeedback();
            pharmaPartners.appreciation();
        }
    }

}


@Component
class Student {

    public boolean isInterested() {
        return Boolean.TRUE;
    }

    @NonNull
    public String givesEmailAddress() {
        return "StudentsAwesomeEmailAddressGivenByUniversity";
    }

    @NonNull
    public String hopefullyGivesFeedback() {
        return "StudentsAwesomeFeedback";
    }

}


@Component
class PharmaPartners {

    private static final String pharmaPartnersEmailAddress = "pharma@partners.nl";
    private static final String host = "SomeIPv6Address";

    @NonNull
    public String appreciation() {
        return "Thank you!";
    }

    public void sendInternshipAssignments(@NonNull String emailAddress) {
        final Properties properties = System.getProperties();

        properties.setProperty("mail.smtp.host", host);
        final Session session = Session.getDefaultInstance(properties);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(pharmaPartnersEmailAddress));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));
            message.setSubject("InternShip!");
            message.setText("Hi Student, wat ontzettend gaaf dat jij ons komt helpen de zorg te verbeteren!");
//			Transport.send(message);
            System.out.print(message.getContent());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void arrangeMeeting(@NonNull String emailAddress) {
        final Properties properties = System.getProperties();

        properties.setProperty("mail.smtp.host", host);
        final Session session = Session.getDefaultInstance(properties);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(pharmaPartnersEmailAddress));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));
            message.setSubject("InternShip, let's get acquainted!");
            message.setText("Hi Student, wat leuk dat je voor PharmaPartners hebt gekozen, laten we nader kennismaken!");
//			Transport.send(message);
            System.out.print(message.getContent());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setupInternshipAgreement(@NonNull String emailAddress) {
        final Properties properties = System.getProperties();

        properties.setProperty("mail.smtp.host", host);
        final Session session = Session.getDefaultInstance(properties);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(pharmaPartnersEmailAddress));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));
            message.setSubject("InternShip, let's get started!");
            message.setText("Hi Student, we gaan samen gave dingen maken!");
//			Transport.send(message);
            System.out.print(message.getContent());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}


@Repository
class InternshipRepository {

    public void save(@NonNull Student student) {
        System.out.println("Student has been assigned to an assessment");
    }

}
